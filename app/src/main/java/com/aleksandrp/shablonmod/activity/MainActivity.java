package com.aleksandrp.shablonmod.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.aleksandrp.shablonmod.R;
import com.aleksandrp.shablonmod.fragmenrt.BaseFragment;
import com.aleksandrp.shablonmod.fragmenrt.DescriptionFragment;
import com.aleksandrp.shablonmod.fragmenrt.DisclaimerFragment;
import com.aleksandrp.shablonmod.fragmenrt.DownloadFragment;
import com.aleksandrp.shablonmod.fragmenrt.InfoFragment;
import com.aleksandrp.shablonmod.fragmenrt.MainFragment;
import com.aleksandrp.shablonmod.utils.Ad;
import com.aleksandrp.shablonmod.utils.DownloadHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.util.Locale;

import static com.aleksandrp.shablonmod.utils.Ad.md5;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progress;

    private Fragment fragment;
    private MainFragment mainFragment;
    private InfoFragment infoFragment;
    private DisclaimerFragment disclaimerFragment;
    private DescriptionFragment descriptionFragment;
    private DownloadFragment downloadFragment;

    // check permissions
    public int REQUEST_ACCESS_FINE_LOCATION = 0x111;
    private String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    private InterstitialAd mInterstitialAd;
    private RewardedVideoAd mRewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUi();

        fragmentManager = getSupportFragmentManager();

        new Ad().showBanner(this, new Ad.ListenerLoadAd() {
            @Override
            public void isLoadBanner(Boolean load) {
                if (load) {
                    startMainFragment();
                } else {
                    progress.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, "Need Internet for connection!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startMainFragment();
            }
        }, 15000);


        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.big_banner_id));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                Ad.requestNewInterstitial(mInterstitialAd, MainActivity.this);
            }
        });
        Ad.requestNewInterstitial(mInterstitialAd, this);
        // Use an activity context to get the rewarded video instance.
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {

            @Override
            public void onRewarded(RewardItem reward) {
                Log.d("VIDEO_", "onRewarded! currency: ${reward.type} amount: ${reward.amount}");
                // Reward the user.
                DownloadHelper.downloadAndOpenFile(MainActivity.this);
            }

            @Override
            public void onRewardedVideoAdLeftApplication() {
                Log.d("VIDEO_", "onRewardedVideoAdLeftApplication");
            }

            @Override
            public void onRewardedVideoAdClosed() {
                Log.d("VIDEO_", "onRewardedVideoAdClosed");
                loadRewardedVideoAd();
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode) {
                Log.d("VIDEO_", "onRewardedVideoAdFailedToLoad  " + errorCode);
                if (errorCode == 0) {
                    loadRewardedVideoAd();
                }
            }

            @Override
            public void onRewardedVideoAdLoaded() {
                Log.d("VIDEO_", "onRewardedVideoAdLoaded");
            }

            @Override
            public void onRewardedVideoAdOpened() {
                Log.d("VIDEO_", "onRewardedVideoAdOpened");
            }

            @Override
            public void onRewardedVideoStarted() {
                Log.d("VIDEO_", "onRewardedVideoStarted");
            }

            @Override
            public void onRewardedVideoCompleted() {
                Log.d("VIDEO_", "onRewardedVideoCompleted");
            }
        });
    }

    @Override
    public void onBackPressed() {
        showFullAd();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    @Override
    public void onResume() {
        mRewardedVideoAd.resume(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        mRewardedVideoAd.pause(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mRewardedVideoAd.destroy(this);
        super.onDestroy();
    }


    private void startMainFragment() {
        if (fragment == null) {
            changeFragment(BaseFragment.FragmentEnum.MAIN_FRAGMENT);
        }
        progress.setVisibility(View.GONE);
    }

    public void changeFragment(BaseFragment.FragmentEnum anEnum) {
        switch (anEnum) {
            case MAIN_FRAGMENT:
                changeFragment(mainFragment);
                break;
            case INFO_FRAGMENT:
                showFullAd();
                changeFragment(infoFragment);
                break;
            case DESCRIPTION_FRAGMENT:
                showFullAd();
                changeFragment(descriptionFragment);
                break;
            case DISCLAIMER_FRAGMENT:
                showFullAd();
                changeFragment(disclaimerFragment);
                break;
            case DOWNLOAD_FRAGMENT:
                showFullAd();
                changeFragment(downloadFragment);
                break;
        }
    }

    private void changeFragment(Fragment fragment) {
        if (this.fragment == fragment) {
            return;
        }
        this.fragment = fragment;
        try {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();
        } catch (Exception e) {
        }
    }

    private void initUi() {
        progress = findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);

        mainFragment = new MainFragment();
        infoFragment = new InfoFragment();
        descriptionFragment = new DescriptionFragment();
        downloadFragment = new DownloadFragment();
        disclaimerFragment = new DisclaimerFragment();
    }

    //    ================== Permissions ====================
    public boolean checkPermissins() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String perms : permissions) {
                int res = checkCallingOrSelfPermission(perms);
                if (res != PackageManager.PERMISSION_GRANTED) {
                    requestPerms();
                    return false;
                }
            }
        }
        return true;
    }

    private void requestPerms() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //reload my activity with permission granted or use the features what required the permission
                showRewardedVideoAd();
            } else {
                showNoStoragePermissionSnackbar();
            }
        }
    }

    private void showNoStoragePermissionSnackbar() {
        Snackbar.make(
                progress,
                "Please confirm permission to read and write data",
                Snackbar.LENGTH_LONG)
                .setAction("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openApplicationSettings();
                    }
                })
                .show();
    }

    private void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:$packageName"));
        startActivityForResult(appSettingsIntent, REQUEST_ACCESS_FINE_LOCATION);
    }

    public void makeDownload() {
        DownloadHelper.downloadAndOpenFile(this);
    }

    public void showRewardedVideoAd() {
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        } else {
            showFullAd();
            makeDownload();
        }
    }

    public void showFullAd() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    public void loadRewardedVideoAd() {
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceId = md5(android_id).toUpperCase(Locale.ENGLISH);

        AdRequest adRequest = new AdRequest.Builder()
//            .addTestDevice(deviceId)
                .build();
        mRewardedVideoAd.loadAd(getString(R.string.app_id_video), adRequest);
    }
}

