package com.aleksandrp.shablonmod.fragmenrt;

import android.view.View;

import com.aleksandrp.shablonmod.R;
import com.aleksandrp.shablonmod.activity.MainActivity;

public class MainFragment extends BaseFragment {


    @Override
    int getIntFragment() {
        return R.layout.fragment_main;
    }

    @Override
    void initUI(View view) {
        view.findViewById(R.id.tv_go_to_disclaimer).setOnClickListener(this);
        view.findViewById(R.id.tv_go_to_info).setOnClickListener(this);
        view.findViewById(R.id.tv_go_to_description).setOnClickListener(this);
    }

    @Override
    void getNextFragment(View view) {
        switch (view.getId()) {
            case R.id.tv_go_to_disclaimer:
                ((MainActivity) getActivity()).changeFragment(FragmentEnum.DISCLAIMER_FRAGMENT);
                break;

            case R.id.tv_go_to_info:
                ((MainActivity) getActivity()).changeFragment(FragmentEnum.INFO_FRAGMENT);
                break;

            case R.id.tv_go_to_description:
                ((MainActivity) getActivity()).changeFragment(FragmentEnum.DESCRIPTION_FRAGMENT);
                break;

        }
    }
}
