package com.aleksandrp.shablonmod.fragmenrt;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    public BaseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(getIntFragment(), container, false);
        initUI(view);
        return view;
    }


    @Override
    public void onClick(View view) {
        getNextFragment(view);
    }


    abstract int getIntFragment();

    abstract void initUI(View view);

    abstract void getNextFragment(View view);


    public enum FragmentEnum {
        MAIN_FRAGMENT,
        INFO_FRAGMENT,
        DESCRIPTION_FRAGMENT,
        DISCLAIMER_FRAGMENT,
        DOWNLOAD_FRAGMENT
    }
}


