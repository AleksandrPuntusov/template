package com.aleksandrp.shablonmod.fragmenrt;

import android.view.View;
import android.widget.TextView;

import com.aleksandrp.shablonmod.R;
import com.aleksandrp.shablonmod.activity.MainActivity;

public class DownloadFragment extends BaseFragment {

    @Override
    int getIntFragment() {
        return R.layout.fragment_download;
    }

    @Override
    void initUI(View view) {
        view.findViewById(R.id.tv_go_to_download).setOnClickListener(this);
    }

    @Override
    void getNextFragment(View view) {
        switch (view.getId()) {
            case R.id.tv_go_to_download:
                if (((MainActivity) getActivity()).checkPermissins()) {
                    ((MainActivity) getActivity()).showRewardedVideoAd();
                }
                break;
        }
    }

}
