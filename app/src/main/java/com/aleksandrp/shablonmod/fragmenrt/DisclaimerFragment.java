package com.aleksandrp.shablonmod.fragmenrt;

import android.view.View;
import android.widget.TextView;

import com.aleksandrp.shablonmod.R;
import com.aleksandrp.shablonmod.activity.MainActivity;

public class DisclaimerFragment extends BaseFragment {


    @Override
    int getIntFragment() {
        return R.layout.fragment_info;
    }

    @Override
    void initUI(View view) {
        TextView tv_title_info = (TextView) view.findViewById(R.id.tv_title_info);
        tv_title_info.setText(getString(R.string.disclaimer));
        TextView tv_info = (TextView) view.findViewById(R.id.tv_info);
        tv_info.setText(getString(R.string.disclaimer_app));
        TextView tv_go_to_download = (TextView) view.findViewById(R.id.tv_go_to_download);
        tv_go_to_download.setText(getString(R.string.description));

        view.findViewById(R.id.tv_go_to_download).setOnClickListener(this);
    }

    @Override
    void getNextFragment(View view) {
        switch (view.getId()) {
            case R.id.tv_go_to_download:
                ((MainActivity) getActivity()).changeFragment(FragmentEnum.DESCRIPTION_FRAGMENT);
                break;
        }
    }
}
