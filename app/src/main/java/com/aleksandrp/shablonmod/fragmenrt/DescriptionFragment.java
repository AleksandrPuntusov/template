package com.aleksandrp.shablonmod.fragmenrt;

import android.view.View;
import android.widget.TextView;

import com.aleksandrp.shablonmod.R;
import com.aleksandrp.shablonmod.activity.MainActivity;

public class DescriptionFragment extends BaseFragment {

    @Override
    int getIntFragment() {
        return R.layout.fragment_info;
    }

    @Override
    void initUI(View view) {
        TextView tv_title_info = (TextView) view.findViewById(R.id.tv_title_info);
        tv_title_info.setText(getString(R.string.description));
        TextView tv_info = (TextView) view.findViewById(R.id.tv_info);
        tv_info.setText(getString(R.string.description_mod));

        view.findViewById(R.id.tv_go_to_download).setOnClickListener(this);
    }

    @Override
    void getNextFragment(View view) {
        switch (view.getId()) {
            case R.id.tv_go_to_download:
                ((MainActivity) getActivity()).changeFragment(FragmentEnum.DOWNLOAD_FRAGMENT);
                break;
        }
    }
}
