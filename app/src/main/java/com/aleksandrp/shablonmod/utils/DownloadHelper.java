package com.aleksandrp.shablonmod.utils;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;

import com.aleksandrp.shablonmod.R;

import java.io.File;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class DownloadHelper {


    //    https://drive.google.com/file/d/1awvPHexNwU9g6oV0-IRm7O-WDx9QLDdR/view?usp=sharing
    public static final String API_BASE_URL = "https://drive.google.com/uc?export=download&confirm=no_antivirus&id=";
    public static final String API_BASE_ID_FILE = "1awvPHexNwU9g6oV0-IRm7O-WDx9QLDdR";
    public static final String EXTENSION = "mcworld";
    public static final String NAME_FILE = "Mod_pack";

    public static void downloadAndOpenFile(Context context) {
        // Get filename
        String filename = NAME_FILE + "." + EXTENSION;
        // The place where the downloaded PDF file will be put
        File filesDir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        File tempFile = new File(filesDir, filename);
        String fileUrl = API_BASE_URL + API_BASE_ID_FILE;

        if (tempFile.exists()) {
            tempFile.delete();
        }

        // Show progress dialog while downloading
        final ProgressDialog progress = ProgressDialog.show(
                context,
                context.getString(R.string.download),
                context.getString(R.string.waight), true);

        // Create the download request
        DownloadManager.Request r = new DownloadManager.Request(Uri.parse(fileUrl));
        r.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_DOWNLOADS, filename);
        final DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!progress.isShowing()) {
                    return;
                }
                context.unregisterReceiver(this);

                progress.dismiss();
                long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                Cursor c = dm.query(new DownloadManager.Query().setFilterById(downloadId));

                if (c.moveToFirst()) {
                    int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    if (status == DownloadManager.STATUS_SUCCESSFUL) {
//                            openFile(context, tempFile)
                    }
                }
                c.close();
            }
        };
        context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        // Enqueue the request
        dm.enqueue(r);
    }

    /**
     * Open a local file with an installed reader
     *
     * @param context
     * @param localUri
     */
    public void openFile(Context context, File localUri) {

        String mimeType = "application/" + EXTENSION;
        Intent install = new Intent(Intent.ACTION_VIEW);
        install.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            // Old Approach
            install.setDataAndType(Uri.fromFile(localUri), mimeType);
            // End Old approach
        } else {
            // New Approach
            Uri apkURI = FileProvider.getUriForFile(
                    context,
                    context.getApplicationInfo()
                            .packageName + ".provider", localUri);
            install.setDataAndType(apkURI, mimeType);
            install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            // End New Approach
        }
        context.startActivity(install);
    }

}
