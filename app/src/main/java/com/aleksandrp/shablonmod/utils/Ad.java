package com.aleksandrp.shablonmod.utils;

import android.app.Activity;
import android.provider.Settings;
import android.view.View;

import com.aleksandrp.shablonmod.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class Ad {

    private ListenerLoadAd listener;

    public void showBanner(final Activity activity, ListenerLoadAd listener_) {
        this.listener = listener_;

        String android_id = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceId = md5(android_id).toUpperCase(Locale.ENGLISH);

        MobileAds.initialize(activity, activity.getString(R.string.app_id));
        final AdView banner = activity.findViewById(R.id.banner);
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(deviceId)
                .build();
        banner.loadAd(adRequest);

        banner.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                setupContentViewPadding(activity, banner.getHeight());
                if (listener != null) {
                    listener.isLoadBanner(true);
                }
            }
        });
    }


    public static void setupContentViewPadding(Activity activity, int padding) {
        try {
            View view = activity.findViewById(R.id.fragment_container);
            view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), padding);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void requestNewInterstitial(InterstitialAd mInterstitialAd, Activity mActivity) {
        if (!mInterstitialAd.isLoaded()) {
            //get EMULATOR deviceID todo потом надо удалить
            String android_id = Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
            String deviceId = md5(android_id).toUpperCase(Locale.ENGLISH);
            AdRequest adRequest = new AdRequest.Builder()
//                                        .addTestDevice(deviceId)
                    .build();
            mInterstitialAd.loadAd(adRequest);
        }
    }

    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            //Logger.logStackTrace(TAG,e);
            System.out.println(e.getLocalizedMessage());
        }
        return "";
    }


    public static interface ListenerLoadAd {
        void isLoadBanner(Boolean load);
    }

}
